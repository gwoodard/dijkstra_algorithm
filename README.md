# Dijkstra's Algorithm #

This repo represents the source code for a HW assignment based on Dijkstra's Algorithm. Dijkstra's Algorithm is a solution to the single-source shortest path problem in graph theory. It works on both directed and undirected graphs. However, all edges must have nonnegative weights.

**Approach:** Greedy Algo

**Input:** Weighted graph G = {E, V} and source vertex veV, such that all edge weights are nonnegative

**Output:** Lengths of shortest paths (or the shortest paths themselves) from a given source vertex to all other vertices

**Language:** Java